import random

MAX_LINE=3
MAX_BET=100000
MIN_BET=100

ROWS=5
COLS=5

print("Welcome to Malibu Club!")

reel_symbols={
    "A": 1,
    "B": 3,
    "C": 5,
    "D": 7,
    "E": 2,
    "F": 8
}

reel_value={
    "A": 8,
    "B": 4,
    "C": 5,
    "D": 6,
    "E": 7,
    "F": 1
}

def check_winnings(columns, values, lines, bet):
    winning=0
    winning_line=[]
    for line in range(lines):
        symbol=columns[0][line]
        for column in columns:
            symbol_to_check=column[line]
            if symbol!=symbol_to_check:
                break
        else:
            winning+=values[symbol]*bet
            winning_line.append(lines+1)
    return winning,winning_line

def slot_machine_spin(rows,cols,symbols):
    all_symbols=[]     # This line code will store all the required symbol for the slot machine.
    for symbol,reel_symbols in symbols.items():     # This will store the 'Keys' and 'Values' from reel symbols separately in 'symbol' and 'reel_symbols' respectively.
        for _ in range(reel_symbols):      # This loop will go through from the 'Values' of reel_symbols.
            all_symbols.append(symbol)     # This will append the all_symbols with all 'Keys' by the number of times of their 'Values' because 'reel_symbols' contains all the 'Values' for the 'Keys' 
    
    columns=[]     # This list contains entire values inside of a columns.
    for _ in range(cols):     # This loop will generate all the symbols for every column.
        column=[]     # This list will contain what value will go into a single column.
        current_symbols=all_symbols[:]     # This statement will copy the all_symbols list into current_symbols.
        for _ in range(rows):     # This loop will generate the number of column according to the number of rows.
            value=random.choice(current_symbols)     # This will generate the random values for our columns.
            current_symbols.remove(value)      # This will remove the repetative values from a each columns if they had arrived before in the bet, which will be the first instance of the list.
            column.append(value)     # This will append the column with the new sets of values.
        columns.append(column)     # This will eventually append the columns, which contains entire sets of values of columns with the new sets of values got from the column list.
    return columns

def print_slot_machine(columns):
    for row in range(len(columns[0])):
        for i, column in enumerate(columns):
            if i != len(columns)-1:
                print(column[row],end=" | ")
            else:
                print(column[row],end="")
        print()

# This function takes the deposite balance amount from the user to bet.
def deposit():
    while True:
        try:
            cash = int(input("What would you like to deposit? INR "))
            if cash > 0:
                break
            else:
                print("Enter a number greater than zero.")
        except ValueError:
            print("Enter a valid number.")
    return cash

# This function will define the number of lines to bet on, taken from the user.
def get_number_of_lines():
    while True:
        try:
            line = int(input("Enter the number of lines to bet (1-" + str(MAX_LINE) + ")? "))
            if 1<=line<=MAX_LINE:
                break
            else:
                print("Enter the valid number of lines.")
        except ValueError:
            print("Enter a valid number of lines.")
    return line

# This function will define the bet amount on each line, taken from the user.
def bet():
    while True:
        try:
            amount = int(input("Enter the amount of bet on each lines: "))
            if MIN_BET<=amount<=MAX_BET:
                break
            else:
                print("Amount must be between (" + str(MIN_BET) + " and " + str(MAX_BET) + ").")
        except ValueError:
            print("Enter a valid number.")
    return amount

# This function will run almost all the primary work for our gambling machine.
def spin(balance):
    line=get_number_of_lines()
    while True:
        gamble=bet()
        total_bet=gamble*line
        if total_bet>balance:
            print("You don't have enough to bet that amount, your initial balance was (" + str(balance) + ")")
        else:
            break
    print("Your bet is " + str(gamble) + " on " + str(line) + " lines. Total bet is equal to " + str(total_bet) + ".")
    slot=slot_machine_spin(ROWS,COLS,reel_symbols)
    print_slot_machine(slot)
    winnings,winning_line=check_winnings(slot,reel_value,line,bet)
    print("You won INR " + str(winnings) + ".")
    print("You won on lines: ", *winning_line)
    return winnings-total_bet

def main():
    balance=deposit()
    while True:
        print("Your current balance is: " + str(balance) + ".")
        answer=input("Press enter to play again or 'q' to quit: ")
        if answer == 'q':
            break
        balance+=spin(balance)

main()