# Malibu Club: An Classic Slot Game!

Malibu Club is a game implemented in Python that emulates the excitement of a traditional slot machine. The player can stake from INR 100 to INR 100000 coins and reap rewards based on the alignment of six symbols (A, B, C, D, E, F) on each of the five reels. The game persists until the player exhausts their coins or opts to terminate the game.

Created By **Ayush Bhattacharya**, Inspired by [Tech with Tim](https://t.ly/XrDZZ)!
